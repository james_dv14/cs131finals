/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cs131finals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class InputHelper {

  /**
   * 
   * @param def A default value to use if input is faulty.
   * @return An integer value parsed from console input.
   */
  public static int getIntFromUser(int def) {
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    int value;
    try {
      value = Integer.parseInt(in.readLine());
    } 
    catch (IOException | NumberFormatException e) 
    {
      value = def;
    }

    return value;
  }
  
  /**
   * 
   * @param def A default value to use if input is faulty.
   * @return A double value parsed from console input.
   */
  public static double getDoubleFromUser(double def) {
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    double value;
    try {
      value = Double.parseDouble(in.readLine());
    } 
    catch (IOException | NumberFormatException e) 
    {
      value = def;
    }

    return value;
  }
  
  /**
   * 
   * @param def A default value to use if input is faulty.
   * @return A string value parsed from console input.
   */
  public static String getStringFromUser(String def) {
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    String value;
    try {
      value = in.readLine();
      if (value.isEmpty()) value = def;
    } 
    catch (IOException e)
    {
      value = def;
    }

    return value;
  }
  
}
