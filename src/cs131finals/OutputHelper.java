/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cs131finals;

import cs131finals.base.NumericalMethod;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class OutputHelper {
  
  public static void writeIterationsToFile(NumericalMethod solver, String path) {
    PrintWriter output;
    File outfile = new File(path);
    
    try {
      output = new PrintWriter(new FileWriter(outfile, false));
      
      for (int i = 0; i < solver.vector_log.size(); i++) {
        output.print(solver.h * i);
        output.print(", ");
        output.print(solver.vector_log.get(i).get(0));
        output.println();
      }
      
      output.close();
    }
    catch (IOException e) {
      System.err.println("Invalid output file.");
    }
    catch (Exception e) {
      System.err.println("Unknown error occurred.");
      e.printStackTrace();
    }
    
  }

}