/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cs131finals.function;

import cs131finals.base.Vector;
import cs131finals.base.VectorFunction;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ProbeOscillation implements VectorFunction {
  double b;
  double k;
  double a;
  double m;
  double g;
  
  public ProbeOscillation(double m, double g, double a, double k, double b) {
    this.m = m;
    this.g = g;
    this.a = a;
    this.k = k;
    this.b = b;
  }
  
  @Override
  public Vector evaluate(Vector v) {
    Vector result = new Vector(v.size());
    result.set(0, v.get(1));
    
    // tmp = -bx'
    double tmp = -1 * b * v.get(1);
    // tmp = -bx' - kx
    tmp -= k * v.get(0);
    // tmp = -bx' - kx - a*x^3
    tmp -= a * Math.pow(v.get(0), 3);
    // tmp = -bx' - kx - a*x^3 - mg
    tmp -= m * g;
    // tmp = (-bx' - kx - a*x^3 - mg) / m
    tmp /= m;
    
    result.set(1, tmp);
    return result;
  }

}
