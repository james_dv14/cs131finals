/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cs131finals.function;

import cs131finals.base.Vector;
import cs131finals.base.VectorFunction;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class TestFunction implements VectorFunction {

  @Override
  public Vector evaluate(Vector v) {
    Vector result = new Vector(v.size());
    result.set(0, v.get(1));
    result.set(1, -4 * v.get(1) - 3 * v.get(0));
    return result;
  }

}