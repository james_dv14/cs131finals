package cs131finals.method;
import cs131finals.base.*;
import java.util.ArrayList;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class RK4 extends NumericalMethod {
  Vector[] k;
  ArrayList<Vector[]> k_log;
  
  public RK4(VectorGiven given, double h) {
    super(given, h);
    vector_log = new ArrayList();
    k_log = new ArrayList();
  }
  
  
  /**
   * x_{n+1} = x_n + h/6 * (k1 + 2*k2 + 2*k3 + k4)
   * 
   * @param current Values of the input vector at the current time step.
   * @param f A function that is the derivative of the vector.
   * @param h Step size.
   * @return The values of the input vector at the next time step in RK4 
   * method (i.e. x_{n+1}).
   */
  @Override
  protected Vector next(Vector current, VectorFunction f, double h) {
    Vector tmp = new Vector(current);
    k = new Vector[4];
    
    // f(x) = x'
    k[0] = f.evaluate(tmp); // k1 = f(x)
    
    for (int i = 0; i < current.size(); i++)
      tmp.set(i, current.get(i) + h / 2 * k[0].get(i));
    k[1] = f.evaluate(tmp); // k2 = f(x + h/2 * k1)
    
    for (int i = 0; i < current.size(); i++)
      tmp.set(i, current.get(i) + h / 2 * k[1].get(i));
    k[2] = f.evaluate(tmp); // k3 = f(x + h/2 * k2)
    
    for (int i = 0; i < current.size(); i++)
      tmp.set(i, current.get(i) + h * k[2].get(i));
    k[3] = f.evaluate(tmp); // k4 = f(x + h * k3)
    
    tmp = k[0].add(k[1].mult(2)); // tmp = k1 + 2*k2
    tmp = tmp.add(k[2].mult(2));  // tmp = k1 + 2*k2 + 2*k3
    tmp = tmp.add(k[3]);          // tmp = k1 + 2*k2 + 2*k3 + k4
    tmp = tmp.mult(h / 6);        // tmp = (h/6) (k1 + 2*k2 + 2*k3 + k4)
    return current.add(tmp);      // return x_n + h/6 * (k1 + 2*k2 + 2*k3 + k4)
  }
  
  @Override
  protected void logIteration() {
    super.logIteration();
    k_log.add(k);
  }
  
  @Override
  public String toString() {
    String str = "";
    
    for (int i = 0; i < vector_log.size(); i++) {
      str += String.format("x(%.5f) = ", h * i);
      str += vector_log.get(i);
      str += "\n";
      if (i < vector_log.size() - 1) {
        for (int j = 0; j < 4; j++) {
          str += String.format("  k%d =", j);
          str += k_log.get(i)[j];
          str += "\n";
        }
      }
      str += "\n";
    }
    
    return str.trim();
  }
}
