package cs131finals.base;

import java.util.ArrayList;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public abstract class NumericalMethod {
  protected Vector current;
  
  public VectorGiven given;
  public double h;
  public ArrayList<Vector> vector_log;
  
  /**
   * 
   * @param given Common given for vectors: the initial values, the vector 
   * function, and the starting and ending values for the independent variable.
   * @param h Step size.
   */
  public NumericalMethod(VectorGiven given, double h) {
    this.given = given;
    this.h = h;
  }
  
  /**
   * 
   * @return The largest difference between an element of the vector and its
   * value in the previous iteration.
   */
  public double solve() {
    current = new Vector(given.v);
    double max_diff = Double.MAX_VALUE;
    
    logInitial();
    
    for (double i = given.a; i < given.b; i += h) {
      Vector previous = new Vector(current);
      current = next(current, given.f, h);
      max_diff = current.sub(previous).max();
      logIteration();
    }
    
    return max_diff;
  }
  
  /**
   * Override to add custom logic for logging initial data before loop.
   */
  protected void logInitial() {
    vector_log.add(current);
  }
  
  
  /**
   * Override to add custom logic for logging computation data after each loop 
   * iteration.
   */
  protected void logIteration() {
    vector_log.add(current);
  }
  
  /**
   * Insert subroutine for obtaining the values of the next iteration here.
   * @param current The current values of the dependent variable.
   * @param f A vector function.
   * @param h Step size.
   * @return 
   */
  protected abstract Vector next(Vector current, VectorFunction f, double h);

}