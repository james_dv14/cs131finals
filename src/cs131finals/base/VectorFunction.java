package cs131finals.base;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public interface VectorFunction {
  
  /**
   * 
   * @param v An input vector.
   * @return A new vector that is the result of applying the overriding function
   * to the input.
   */
  public Vector evaluate(Vector v);

}
