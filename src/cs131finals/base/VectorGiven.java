/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cs131finals.base;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class VectorGiven {
  public Vector v;
  public VectorFunction f;
  public double a;
  public double b;
  public String i_var;
  
  /**
   * Wraps common given values into a structure (for shorter argument lists when
   * passing to methods).
   * @param v The initial values of the dependent variable.
   * @param f A vector function.
   * @param a The starting value of the independent variable.
   * @param b The final value of the independent variable.
   */
  public VectorGiven(Vector v, VectorFunction f, double a, double b) {
    this(v, f, a, b, "x");
  }
  
  /**
   * Wraps common given values into a structure (for shorter argument lists when
   * passing to methods).
   * @param v The initial values of the dependent variable.
   * @param f A vector function.
   * @param a The starting value of the independent variable.
   * @param b The final value of the independent variable.
   * @param i_var The name of the independent variable.
   */
  public VectorGiven(Vector v, VectorFunction f, 
          double a, double b, String i_var) {
    this.v = v;
    this.f = f;
    this.a = a;
    this.b = b;
    this.i_var = i_var;
  }
}