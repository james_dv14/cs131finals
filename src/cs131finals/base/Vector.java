package cs131finals.base;

import java.util.Arrays;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Vector {
  protected double[] v;
  
  /**
   * Initializes a vector of the given size with all elements set to 0.
   * @param size The number of vector elements.
   */
  public Vector(int size) {
    this.v = new double[size];
  }
  
  /**
   * Initializes the vector as a copy of an existing array.
   * @param v An existing vector in array
   */
  public Vector(double[] v) {
    this.v = Arrays.copyOf(v, v.length);
  }
  
  /**
   * Initializes the vector as a copy of an existing Vector.
   * @param orig The original vector.
   */
  public Vector(Vector orig) {
    this.v = Arrays.copyOf(orig.v, orig.v.length);
  }
  
  /**
   * 
   * @param i An index.
   * @return The element at the index.
   */
  public double get(int i) { return v[i]; }
  
  /**
   * Sets the value of the element at the index to the given value.
   * @param i An index.
   * @param x The new value.
   */
  public void set(int i, double x) { v[i] = x; }
  
  /**
   * 
   * @return The number of elements in the vector.
   */
  public int size() { return v.length; }
  
  /**
   * 
   * @return A string with the vector's elements enclosed by brackets and
   * separated with commas.
   */
  @Override
  public String toString() { return this.toString(5); }
  
  /**
   * 
   * @param precision The number of decimal places to display.
   * @return A string with the vector's elements (at the given precision) 
   * enclosed by brackets and separated with commas.
   */
  public String toString(int precision) {
    String str = "[";
    for (double e : v) {
      str += String.format("%." + precision + "f", e);
      str += ", ";
    }
    str = str.substring(0, str.lastIndexOf(", "));
    str += "]";
    return str;
  }
  
  /**
   * 
   * @param a A scalar operand.
   * @return A new vector that is the product of the calling vector and operand.
   */
  public Vector mult(double a) {
    Vector result = new Vector(v);
    for (int i = 0; i < v.length; i++)
      result.set(i, v[i] * a);
    return result;
  }
  
  /**
   * 
   * @param other Another vector.
   * @return A new vector that is the sum of the two vectors.
   */  
  public Vector add(Vector other) {
    Vector result = new Vector(this.v);
    for (int i = 0; i < this.v.length; i++)
      result.set(i, this.v[i] + other.v[i]);
    return result;
  }
  
  /**
   * 
   * @param other Another vector.
   * @return A new vector that is the difference of the two vectors.
   */  
  public Vector sub(Vector other) {
    return this.add(other.mult(-1));
  }
    
  /**
   * 
   * @return The largest among the vector's elements.
   */
  public double max() {
    double max = Double.MIN_VALUE;
    for (double e : v)
      if (e > max) max = e;
    return max;
  }

}