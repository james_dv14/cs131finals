package cs131finals;

import cs131finals.base.*;
import cs131finals.function.ProbeOscillation;
import cs131finals.method.RK4;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Main {
  public static final String OUTPUT_DIR = "graph/";
  
  public static final double M = 1220.0;
  public static final double G = 17.5;
  public static final double A = 450000.0;
  public static final double K = 35600.0;
  public static double b = 1000;
  
  public static void main(String[] args) {
    // initialize defaults for input
    double b_result = -1;
    double b_low = 1000.0;
    double b_high = 10000.0;
    double b_inc = 500.0;
    
    double h = 0.01;
    double t_high = 10.0;
    
    boolean stretch;
    
    // display equation governing oscillation
    System.out.println(getOscillationEquationMessage());
    System.out.println();
    
    // accept user input
    System.out.print("Enter low value of b (" + b_low + "): ");
    b_low = InputHelper.getDoubleFromUser(b_low);
    System.out.print("Enter high value of b (" + b_high + "): ");
    b_high = InputHelper.getDoubleFromUser(b_high);
    System.out.print("Enter increment of b (" + b_inc + "): ");
    b_inc = InputHelper.getDoubleFromUser(b_inc);
    
    System.out.print("Enter time step (" + h + "): ");
    h = InputHelper.getDoubleFromUser(h);
    System.out.print("Enter upper bound for time (" + t_high + "): ");
    t_high = InputHelper.getDoubleFromUser(t_high);
    
    // initialize b
    b = b_low;
    
    // iterate through b values from the given low value to the given high value
    while (b <= b_high) {
      // substitute the current b into the equation governing oscillation
      ProbeOscillation osc = new ProbeOscillation(M, G, A, K, b);
      
      // use x(0) = 0 and x'(0) = -5
      double[] arr = {0, -5};
      
      // set time interval to be from 0 to the given upper bound
      VectorGiven given = new VectorGiven(new Vector(arr), osc, 0, t_high);
      
      // apply RK4 method
      RK4 rk4 = new RK4(given, h);
      rk4.solve();
      
      // check if any displacement values are positive
      stretch = hasPositiveValues(rk4);
      
      // record the first (smallest) b value with no positive displacement value
      if (!stretch && b_result < 0) b_result = b;
      
      // write time and displacement values to a file
      String filename = "b_" + (int)b;
      OutputHelper.writeIterationsToFile(rk4, OUTPUT_DIR + filename);
      
      // increment b
      b += b_inc;
    }
    
    // display conclusion
    System.out.println();
    if (b_result < 0) {
      System.out.println("No value in that range will ensure compression "
              + "throughout the oscillation.");
    }
    else {
      System.out.println("The lowest b value that will ensure compression is " 
              + b_result + "N-sec/m.");
    }
  }
  
  //****************************************
  // Check for positive values
  //****************************************
  
  private static boolean hasPositiveValues(NumericalMethod solver) {
    boolean positive = false;
    
    for (Vector v : solver.vector_log)
      if (v.get(0) > 0) positive = true;
    
    return positive;
  }
  
  //****************************************
  // Console messages
  //****************************************
  
  private static String getOscillationEquationMessage() {
    String retstr = "The equation governing the oscillations is:";
    retstr += System.lineSeparator();
    retstr += getOscillationEquation();
    return retstr;
  }
  
  private static String getOscillationEquation() {
    String retstr = "";
    retstr += M + "*x''";
    retstr += " + ";
    retstr += "b*x'";
    retstr += " + ";
    retstr += K + "*x";
    retstr += " + ";
    retstr += A + "*x^3";
    retstr += " = ";
    retstr += "-"+ (M*G);
    return retstr;
  }

}