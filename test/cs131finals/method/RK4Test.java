/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs131finals.method;

import cs131finals.base.*;
import cs131finals.function.TestFunction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author js
 */
public class RK4Test {
  Vector v;
  RK4 solver;
  double v1 = 1.5;
  double v2 = -2.5;
  
  public RK4Test() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    double[] arr = { v1, v2 };
    v = new Vector(arr);
    VectorGiven given = new VectorGiven(v, new TestFunction(), 0, 1.0);
    solver = new RK4(given, 0.5);
  }
  
  @After
  public void tearDown() {
  }
  
  @Test
  public void testNext() {
    Vector result = solver.next(v, new TestFunction(), 0.5);
    
    String truncated = String.format("%.5f", result.get(0));
    assertEquals(truncated, "0.74349");
    
    truncated = String.format("%.5f", result.get(1));
    assertEquals(truncated, "-1.01693");
  }
  
  @Test
  public void testSolve() {
    solver.solve();
    
    Vector last = solver.vector_log.get(solver.vector_log.size() - 1);
    String truncated = String.format("%.5f", last.get(0));
    assertEquals(truncated, "0.40555");
    
    truncated = String.format("%.5f", last.get(1));
    assertEquals(truncated, "-0.48032");
    
    System.out.println(solver);
  }
  
}