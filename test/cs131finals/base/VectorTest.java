package cs131finals.base;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author js
 */
public class VectorTest {
  Vector v;
  double v1 = 1.5;
  double v2 = -2.5;
  
  public VectorTest() {
  }
  
  @BeforeClass
  public static void setUpClass() {
  }
  
  @AfterClass
  public static void tearDownClass() {
  }
  
  @Before
  public void setUp() {
    double[] arr = { v1, v2 };
    v = new Vector(arr);
  }
  
  @After
  public void tearDown() {
  }

  @Test
  public void testConstructorWithSize() {
    v = new Vector(2);
    assertTrue(v.v[0] == 0);
    assertTrue(v.v[1] == 0);
  }
  
  @Test
  public void testConstructorWithArray() {
    assertTrue(v.v[0] == v1);
    assertTrue(v.v[1] == v2);
  }
  
  @Test
  public void testConstructorWithVector() {
    Vector orig = new Vector(2);
    v = new Vector(orig);
    assertTrue(v.v[0] == 0);
    assertTrue(v.v[1] == 0);
  }
  
  @Test
  public void testGet() {
    assertTrue(v.v[0] == v.get(0));
  }
  
  @Test
  public void testSet() {
    v.set(0, 6.9);
    assertTrue(v.v[0] == 6.9);
  }
  
  @Test
  public void testSize() {
    assertTrue(v.size() == 2);
  }
  
  @Test
  public void testToString() {
    assertEquals(v.toString(), String.format("[%.5f, %.5f]", v1, v2));
  }
  
  @Test
  public void testToStringWithPrecision() {
    assertEquals(v.toString(2), String.format("[%.2f, %.2f]", v1, v2));
  }
  
  @Test
  public void testMult() {
    Vector product = v.mult(2);
    assertTrue(product.v[0] == v.v[0] * 2);
    assertTrue(product.v[1] == v.v[1] * 2);
  }
  
  @Test
  public void testAdd() {
    double[] arr = { 0.5, 1.5 };
    Vector other = new Vector(arr);
    Vector sum = v.add(other);
    assertTrue(sum.v[0] == v.v[0] + other.v[0]);
    assertTrue(sum.v[1] == v.v[1] + other.v[1]);
  }
  
  @Test
  public void testMax() {
    v.v[0] = Double.MAX_VALUE;
    assertTrue(v.max() == v.v[0]);
  }
  
  @Test
  public void testSub() {
    double[] arr = { 0.5, 1.5 };
    Vector other = new Vector(arr);
    Vector sum = v.sub(other);
    assertTrue(sum.v[0] == v.v[0] - other.v[0]);
    assertTrue(sum.v[1] == v.v[1] - other.v[1]);
  }
  
}