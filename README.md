* Open a command prompt in the root directory.
* Run:
```
#!bash

$ javac cs131finals/*.java
```
* Run:
```
#!bash

$ java cs131finals.Main
```
* Enter the desired values.
* Navigate to the directory "graph" under this one.
* Open "index.html" to view the graphs. 
* Input "b_<value>" (where value is the value of b you wish to plot) in the text field.
* Click the "Graph!" button.